package br.com.scm.ste.app.endereco.estado;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;

import br.com.insula.opes.hibernate.usertype.ImmutableUserType;

public class EstadoUserType extends ImmutableUserType{

	private static final long serialVersionUID = 1L;

	@Override
	public int[] sqlTypes() {
		return new int[] {Types.VARCHAR};
	}

	@Override
	public Class<?> returnedClass() {
		return Estado.class;
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
			throws HibernateException, SQLException {
		String value = rs.getString(names[0]);
		if (rs.wasNull()) {
			return null;
		} else {
			return Estado.fromCodigo(value);
		}
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws HibernateException, SQLException {
		if (value == null) {
			st.setNull(index, Types.VARCHAR);
		} else {
			Estado estado = (Estado) value;
			st.setString(index, estado.getCodigo());
		}
		
	}

}
