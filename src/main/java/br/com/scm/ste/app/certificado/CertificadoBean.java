package br.com.scm.ste.app.certificado;

import java.io.Serializable;

import br.com.scm.ste.app.aluno.Aluno;

public class CertificadoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Aluno[] selecionados;
	
	public Aluno[] getSelecionados() {
		return selecionados;
	}
	
	public void setSelecionados(Aluno[] selecionados) {
		this.selecionados = selecionados;
	}

}
