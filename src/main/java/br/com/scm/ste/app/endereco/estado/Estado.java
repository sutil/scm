package br.com.scm.ste.app.endereco.estado;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public enum Estado {
	
	AC("Acre"),
	AL("Alagoas"),		
	AP("Amapá"),
	AM("Amazonas"),
	BA("Bahia"),
	CE("Ceará"),
	DF("DistritoFederal"),
	ES("EspíritoSanto"),
	GO("Goiás"),
	MA("Maranhão"),
	MT("MatoGrosso"),
	MS("MatoGrossodoSul"),
	MG("MinasGerais"),
	PA("Pará"),
	PB("Paraíba"),
	PR("Paraná"),
	PE("Pernambuco"),
	PI("Piauí"),
	RJ("RiodeJaneiro"),
	RN("RioGrandedoNorte"),
	RS("RioGrandedoSul"),
	RO("Rondônia"),
	RR("Roraima"),
	SC("SantaCatarina"),
	SP("SãoPaulo"),
	SE("Sergipe"),
	TO("Tocantins");
	
	public String getName(){
		return name();
	}

    private String uf;

    private static final Map<String, Estado> valueMap;

    static {
            Builder<String, Estado> builder = ImmutableMap.builder();

            for (Estado estado : values()) {
                    builder.put(estado.uf, estado);
            }

            valueMap = builder.build();
    }

    private Estado(String codigo) {
            this.uf = codigo;
    }

    public static Estado fromCodigo(String codigo) {
            return valueMap.get(codigo);
    }

    public String getCodigo() {
            return uf;
    }

    public static Map<String, Estado> getValueMap() {
            return valueMap;
    }

}
