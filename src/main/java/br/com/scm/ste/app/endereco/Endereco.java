package br.com.scm.ste.app.endereco;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.insula.opes.Cep;
import br.com.scm.ste.app.endereco.estado.Estado;


@Entity
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cidade_fk")
	private Cidade cidade;
	
	private String logradouro;
	
	private String bairro;
	
	private Cep cep;
	
	private String tipoLogradouro;
	
	Endereco() {
	}
	
	private Endereco (Ac ac){
		this.cidade = Cidade.newInstance(ac.getCidade(), Estado.AC);
		this.cep = Cep.fromString(ac.getCep());
		this.bairro = ac.getBairro();
		this.logradouro = ac.getLogradouro();
		this.tipoLogradouro = ac.getTipoLogradouro();
	}
	
	
}
