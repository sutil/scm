package br.com.scm.ste.app.transferencia;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.DragDropEvent;

public class TransferenciaBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public void onDrop(DragDropEvent event){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Realocando Aluno!")); 
	}
	
	public void confirmar(ActionEvent event){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Aluno Realocado!")); 
	}

}
