package br.com.scm.ste.app.aluno.web;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;

import com.google.common.collect.Lists;

import br.com.scm.ste.app.aluno.Aluno;

@Controller
public class AlunoController {

	public List<Aluno> getAlunos(){
		System.out.println("consultado aluno");
		Aluno a1 = Aluno.newInstance("Edner Zuconelli", new Date(1987, 05, 05), "59658");
		Aluno a2 = Aluno.newInstance("Eduardo Sutil", new Date(1987, 05, 05), "59658");
		Aluno a3 = Aluno.newInstance("Eduardo Trintin", new Date(1987, 05, 05), "59658");
		List<Aluno> alunos = Lists.newLinkedList();
		alunos.add(a1);
		alunos.add(a2);
		alunos.add(a3);
		return alunos;
	}
}
