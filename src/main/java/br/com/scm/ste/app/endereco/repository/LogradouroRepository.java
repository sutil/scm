package br.com.scm.ste.app.endereco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scm.ste.app.endereco.Logradouro;
import br.com.scm.ste.app.repository.ListQueryDslPredicateExecutor;

public interface LogradouroRepository extends JpaRepository<Logradouro, Long>, ListQueryDslPredicateExecutor<Logradouro> {

}
