package br.com.scm.ste.app.matricula;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

public class MatriculaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public List<String> complete(String query) {

		List<String> results = new ArrayList<String>();
		results.add("56025-5  AMANDA DE SOUSA HENRIQUES");
		results.add("60608-3  ANA LUÍZA REGHIN AZEVEDO ROJAS          ");
		results.add("67815-0  ANNA CAROLINA GATTI MORAES              ");
		results.add("68136-7  BRUNO DE DEUS TIRAPELLE                 ");
		results.add("53911-5  CAROLINA VENTURINI TALTASSE             ");
		results.add("55081-0  CAROLINE CORTEZ DE BRITO                ");
		results.add("56487-0  EDERSON CARLOS MARTELIANO               ");
		results.add("59884-3  GUILHERME HASSE UREL                    ");
		results.add("60039-7  ILSINEIA GRAEBIN                        ");
		results.add("64086-5  ISABELA GUALDI FURUYA MATIUSSI          ");
		results.add("70782-0  JULIO CESAR LOURENÇO                    ");
		results.add("70877-5  KÉSIA MARIELY SOARES PAULUCCI           ");
		results.add("71172-5  LETÍCIA DE OLIVEIRA MARCOLINO           ");
		results.add("56502-8  LIN YU CHENG                            ");
		results.add("66869-7  LUCAS SOARES BENITEZ                    ");
		results.add("52896-2  LUCAS THOME                             ");
		results.add("57462-5  MARIANA BASSAN DE MEDEIROS              ");
		results.add("62633-5  MARIANA GUALDI DOS SANTOS               ");
		results.add("57282-7  MARIANE SILVA MARTINI                   ");
		results.add("59782-3  MATEUS MUZULON BRAGA                    ");
		results.add("61219-0  MATHEUS GABRIEL SANTOS FREITAS          ");
		results.add("60874-0  OTAVIO CESAR VECCHI PIERACIO            ");
		results.add("72034-3  PATRICK MICHEL ALVARES                  ");
		results.add("63821-0  PEDRO HENRIQUE DA SILVEIRA              ");
		results.add("61324-5  RAMON SOARES COSTA                      ");
		results.add("53734-9  SAMANTHA AYUMI WATANABE GOMES           ");
		results.add("55896-6  SERGIO HENRIQUE FUJII                   ");
		results.add("69761-7  STEFANINE CUNHA PIVOTO                  ");
		results.add("64601-0  TAÍS VALERIANO NOLASCO VOLPATO          ");
		results.add("57577-3  VITOR CABRAL ZANONI                     ");
		results.add("57974-3  VITOR SEIJI KAMBARA                     ");
		results.add("59488-0  ANDRÉ JANUÁRIO ROCHA                    ");
		results.add("68388-4  ANDRESSA DE MIRANDA BASSAN              ");
		results.add("60194-4  BIANCA CAROLINE TRAVISAN                ");
		results.add("55633-5  DRIELI DIAS DO PRADO                    ");
		results.add("72431-3  EDILAINE MARTINS PEIXOTO                ");
		results.add("66235-6  FABIANA CORTEZ URGNIANI                 ");
		results.add("69596-1  FELIPE VITAL BRAGA VOLPATO              ");
		results.add("69794-8  FERNANDO BRANCO JUNIOR                  ");
		results.add("61587-6  FERNANDO LUTIELI F DE AGUIAR            ");
		results.add("56959-2  GUILHERME ODORICO DA SILVA              ");
		results.add("64949-0  IGOR GUSTAVO DOS SANTOS MONTEIRO        ");
		results.add("59236-3  ISABELA REGINA GRILO SILVA              ");
		results.add("67818-2  JEFERSON CASAGRANDE DE SOUZA            ");
		results.add("61664-7  JESSICA MAYARA CORREIA MISSIAS          ");
		results.add("67625-6  JULIANA KARINA DE O QUEIROZ             ");
		results.add("58279-6  LEALIS VAZ MELEIRO LOPES                ");
		results.add("64437-2  MANOEL HERMES PUPIM NETO                ");
		results.add("68320-8  MARCIO TADEU CAZAQUI DE OLIVEIRA        ");
		results.add("71924-1  MARIA PAULA ROSTELLO DE MELLO           ");
		results.add("67386-5  MATHEUS NARDIM PEREIRA                  ");
		results.add("62953-4  MICHELLI MAYUMI ONO                     ");
		results.add("65596-3  NATÁLIA ALVES RAMOS                     ");
		results.add("51984-2  NATHAN LIYODI NARIAI                    ");
		results.add("65478-0  RENAN BRITO DE OLIVEIRA                 ");
		results.add("63884-0  RENAN FONSECA RAVAGNANI                 ");
		results.add("59015-2  RHUAN FELIPE DE SOUZA THOMAZ            ");
		results.add("65537-2  ROBSON TOCHIO ITO                       ");
		results.add("56074-0  STELLA DE SOUZA DIAMOR                  ");
		results.add("65618-2  TATIANE CRISTINA COUTINHO               ");
		results.add("64214-7  VICTOR FALOPA ANGELI                    ");
		results.add("68151-3  VICTOR FAZION GARCÊZ NOVAIS             ");
		results.add("55046-0  VICTOR KIM EDAGI                        ");
		List<String> retorno = Lists.newLinkedList();

		for (String s : results) {
			if (s.toUpperCase().contains(query.toUpperCase())) {
				retorno.add(s);
			}

		}

		return retorno;
	}

}
