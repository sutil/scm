package br.com.scm.ste.app.endereco;

import org.springframework.web.client.RestTemplate;

import br.com.insula.opes.Cep;

public class SearchingCep {
	
	public String search(Cep cep){
		RestTemplate template = new RestTemplate();
		String response = template.getForObject("http://ceplivre.pc2consultoria.com/index.php?module=cep&cep=87043-220&formato=xml", String.class);
		return response;
	}

}
